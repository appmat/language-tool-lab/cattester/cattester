package ru.appmat

import io.ktor.locations.*

@Location("/api")
class API {
    @Location("/problems")
    data class Problems(val api: API) {
        @Location("/{problemId}")
        data class Problem(val problemId: Long, val problems: Problems) {
            @Location("/tests")
            data class Tests(val problem: Problem) {
                @Location("/{testId}")
                data class Test(val testId: Long, val tests: Tests)

                @Location("/public")
                data class Public(val tests: Tests)
            }

            @Location("/submissions")
            data class Submissions(val problem: Problem) {
                @Location("?env={env}")
                data class SubmissionCreate(val submission: Submission, val env: String)

                @Location("/{submissionId}")
                data class Submission(val submissionId: Long, val submissions: Submissions) {
                    @Location("/submissionFiles")
                    data class SubmissionFiles(val submission: Submission) {
                        @Location("/{submissionFileId}")
                        data class SubmissionFile(val submissionFileId: Long, val submissionFiles: SubmissionFiles)
                    }

                    @Location("/testResults")
                    data class TestResults(val submission: Submission) {
                        @Location("/{testResultId}")
                        data class TestResult(val testResultId: Long, val testResults: TestResults)
                        @Location("/sse")
                        data class SSE(val testResults: TestResults)
                    }
                }
            }
        }
    }
}

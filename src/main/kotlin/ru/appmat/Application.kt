package ru.appmat

import io.ktor.application.*
import ru.appmat.plugins.cors.configureCORS
import ru.appmat.plugins.database.configureDatabase
import ru.appmat.plugins.healthcheck.configureHealthChecks
import ru.appmat.plugins.monitoring.configureMonitoring
import ru.appmat.plugins.routing.configureRouting
import ru.appmat.plugins.serialization.configureSerialization
import ru.appmat.problem_statement.problemAggregateModule
import ru.appmat.submission.submissionAggregateModule

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

/**
 * Please note that you can use any other name instead of *module*.
 * Also note that you can have more then one modules in your application.
 * */
@Suppress("unused") // Referenced in application.conf
@JvmOverloads
fun Application.module(testing: Boolean = false) {
    configureCORS()
    configureRouting()
    val objectMapper = configureSerialization()
    configureMonitoring()
    configureHealthChecks()
    configureDatabase(testing)
    problemAggregateModule()
    submissionAggregateModule(objectMapper)
}

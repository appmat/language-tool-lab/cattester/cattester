package ru.appmat.submission.domain

import java.time.Instant
import java.time.Duration

internal data class TestResult(
    val id: Long,
    val testId: Long,
    val submissionId: Long,
    val resultStatus: String = "unknown",
    val dateTime: Instant = Instant.now(),
    val duration: Duration = Duration.ZERO,
    val errorTitle: String = "",
    val errorOutput: String = "",
    val executionStatus: String = "pending",
    val profileStatistics: String = "",
    val metrics: String = ""
)

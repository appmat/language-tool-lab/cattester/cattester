package ru.appmat.submission.domain

internal data class Submission(
    val id: Long,
    val problemId: Long
)

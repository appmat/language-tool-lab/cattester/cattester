package ru.appmat.submission.domain

internal data class SubmissionAggregate(
    val submission: Submission,
    val submissionFiles: List<SubmissionFile>,
    val testResults: List<TestResult>
) {
    fun getSubmissionFile(submissionFileId: Long): SubmissionFile {
        return submissionFiles.first { it.id == submissionFileId }
    }

    fun getTestResult(testResultId: Long): TestResult {
        return testResults.first { it.id == testResultId }
    }

    fun addTestResult(testResult: TestResult) =
        copy(testResults = testResults.plus(testResult.copy(submissionId = submission.id)))

    fun updateTestResult(testResult: TestResult): SubmissionAggregate {
        val newTestResults = testResults.map {
            if (it.id == testResult.id) testResult.copy(testId = it.testId) else it
        }
        return copy(testResults = newTestResults)
    }
}

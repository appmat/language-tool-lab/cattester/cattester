package ru.appmat.submission.domain

internal data class SubmissionFile(
    val id: Long,
    val submissionId: Long,
    val name: String,
    val file: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SubmissionFile

        if (id != other.id) return false
        if (submissionId != other.submissionId) return false
        if (name != other.name) return false
        if (!file.contentEquals(other.file)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + submissionId.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + file.contentHashCode()
        return result
    }
}

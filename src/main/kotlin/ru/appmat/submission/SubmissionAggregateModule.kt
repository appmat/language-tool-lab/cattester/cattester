package ru.appmat.submission

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.content.*
import io.ktor.locations.*
import io.ktor.locations.post
import io.ktor.locations.put
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.delay
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API
import ru.appmat.problem_statement.repository.*
import ru.appmat.submission.domain.Submission
import ru.appmat.submission.domain.SubmissionAggregate
import ru.appmat.submission.domain.SubmissionFile
import ru.appmat.submission.domain.TestResult
import ru.appmat.submission.repository.*

@Suppress("unused")
fun Application.submissionAggregateModule(objectMapper: ObjectMapper) {
    val submissionService = SubmissionAggregateRepository(
        SubmissionDAO(SubmissionTable), SubmissionFileDAO(SubmissionFileTable), TestResultDAO(TestResultTable)
    )
    val problemService = ProblemAggregateRepository(ProblemStatementDAO(ProblemStatementTable), TestDAO(TestTable))
    routing {
        submissionAggregate(submissionService, problemService, objectMapper)
    }
}

internal fun Routing.submissionAggregate(
    submissionAggregateRepository: SubmissionAggregateRepository,
    problemAggregateRepository: ProblemAggregateRepository,
    objectMapper: ObjectMapper
) {
    get<API.Problems.Problem.Submissions> {
        val submissionList = transaction {
            submissionAggregateRepository.listByProblemId(it.problem.problemId).map { it.submission }
        }
        call.respond(submissionList)
    }

    post<API.Problems.Problem.Submissions> {
        val multipart = call.receiveMultipart()
        val submissionFiles = mutableListOf<SubmissionFile>()
        multipart.forEachPart {
            if (it is PartData.FileItem) submissionFiles.add(
                SubmissionFile(
                    0,
                    0,
                    requireNotNull(it.originalFileName) { "File name has to be present" },
                    it.streamProvider().readBytes()
                )
            )
        }
        val testList = transaction {
            problemAggregateRepository.load(it.problem.problemId).tests
        }
        transaction {
            submissionAggregateRepository.create(
                SubmissionAggregate(
                    Submission(0, it.problem.problemId),
                    submissionFiles,
                    testList.map { TestResult(testId = it.id, id = 0, submissionId = 0) }
                )
            )
        }
        call.respond(Created)
    }

    get<API.Problems.Problem.Submissions.Submission> {
        val submission = transaction {
            submissionAggregateRepository.load(it.submissionId).submission
        }
        call.respond(submission)
    }

    get<API.Problems.Problem.Submissions.Submission.SubmissionFiles> {
        val submissionFileList = transaction {
            submissionAggregateRepository.load(it.submission.submissionId).submissionFiles
        }
        call.respond(submissionFileList)
    }

    get<API.Problems.Problem.Submissions.Submission.SubmissionFiles.SubmissionFile> {
        val submissionFile = transaction {
            submissionAggregateRepository.load(it.submissionFiles.submission.submissionId)
                .getSubmissionFile(it.submissionFileId)
        }
        call.respond(submissionFile)
    }

    get<API.Problems.Problem.Submissions.Submission.TestResults> {
        val testResultList = transaction {
            submissionAggregateRepository.load(it.submission.submissionId).testResults
        }
        call.respond(testResultList)
    }

    post<API.Problems.Problem.Submissions.Submission.TestResults> {
        val testResult = call.receive<TestResult>()
        transaction {
            var submissionAggregate = submissionAggregateRepository.load(it.submission.submissionId)
            submissionAggregate = submissionAggregate.addTestResult(testResult)
            submissionAggregateRepository.save(submissionAggregate)
        }
        call.respond(Created)
    }

    put<API.Problems.Problem.Submissions.Submission.TestResults.TestResult> {
        val testResult = call.receive<TestResult>()
        transaction {
            var submissionAggregate = submissionAggregateRepository.load(it.testResults.submission.submissionId)
            submissionAggregate =
                submissionAggregate.updateTestResult(
                    testResult.copy(
                        id = it.testResultId,
                        submissionId = it.testResults.submission.submissionId
                    )
                )
            submissionAggregateRepository.save(submissionAggregate)
        }
        call.respond(OK)
    }

    get<API.Problems.Problem.Submissions.Submission.TestResults.TestResult> {
        val testResult = transaction {
            submissionAggregateRepository.load(it.testResults.submission.submissionId).getTestResult(it.testResultId)
        }
        call.respond(testResult)
    }

    get<API.Problems.Problem.Submissions.Submission.TestResults.SSE> {
        data class TestStatus(val id: Long, val executionStatus: String) {
            constructor(testResult: TestResult) : this(testResult.id, testResult.executionStatus)
        }
        call.response.cacheControl(CacheControl.NoCache(null))
        call.respondTextWriter(contentType = ContentType.Text.EventStream) {
            while (true) {
                val testResultList = transaction {
                    submissionAggregateRepository.load(it.testResults.submission.submissionId).testResults
                }
                write(
                    "data: ${objectMapper.writeValueAsString(testResultList.map(::TestStatus)).replace("\n", "")}\n\n"
                )
                flush()
                delay(1000L)
            }
        }
    }
}

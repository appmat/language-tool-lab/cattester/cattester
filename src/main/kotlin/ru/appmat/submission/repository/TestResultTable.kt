package ru.appmat.submission.repository

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.javatime.duration

internal object TestResultTable: LongIdTable("test_result") {
    val testId = long("test_id")
    val submissionId = long("submission_id")
    val resultStatus = varchar("result_status", 20)
    val dateTime = timestamp("date_time")
    val duration = duration("duration")
    val errorTitle = varchar("error_title", 20)
    val errorOutput = text("error_output")
    val executionStatus = varchar("execution_status", 20)
    val profileStatistics = text("profile_statistics")
    val metrics = text("metrics")
}

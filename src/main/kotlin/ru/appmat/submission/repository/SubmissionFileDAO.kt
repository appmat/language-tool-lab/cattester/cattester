package ru.appmat.submission.repository

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import ru.appmat.submission.domain.SubmissionFile

internal class SubmissionFileDAO(private val submissionFileTable: SubmissionFileTable) {
    private fun ResultRow.toSubmissionFile() = SubmissionFile(
        id = this[SubmissionFileTable.id].value,
        submissionId = this[SubmissionFileTable.submissionId],
        name = this[SubmissionFileTable.name],
        file = this[SubmissionFileTable.file]
    )

    private fun UpdateBuilder<*>.fromSubmissionFile(submissionFile: SubmissionFile) {
        this[SubmissionFileTable.submissionId] = submissionFile.submissionId
        this[SubmissionFileTable.name] = submissionFile.name
        this[SubmissionFileTable.file] = submissionFile.file
    }

    fun listBySubmissionId(submission_id: Long) =
        submissionFileTable.select {
            SubmissionFileTable.submissionId eq submission_id
        }.mapNotNull {
            it.toSubmissionFile()
        }

    fun create(submissionFile: SubmissionFile) =
        submissionFileTable.insertAndGetId {
            it.fromSubmissionFile(submissionFile)
        }.value
}

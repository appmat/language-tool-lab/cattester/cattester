package ru.appmat.submission.repository

import org.jetbrains.exposed.dao.id.LongIdTable

internal object SubmissionFileTable: LongIdTable("submission_file") {
    val submissionId = long("submission_id")
    val name = text("name")
    val file = binary("file")
}

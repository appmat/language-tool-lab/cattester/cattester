package ru.appmat.submission.repository

import ru.appmat.submission.domain.SubmissionAggregate

internal class SubmissionAggregateRepository(
    private val submissionDAO: SubmissionDAO,
    private val submissionFileDAO: SubmissionFileDAO,
    private val testResultDAO: TestResultDAO
) {
    fun listByProblemId(problem_id: Long): List<SubmissionAggregate> {
        return submissionDAO.listByProblemId(problem_id).map {
            SubmissionAggregate(
                it,
                submissionFileDAO.listBySubmissionId(it.id),
                testResultDAO.listBySubmissionId(it.id)
            )
        }
    }

    fun load(id: Long): SubmissionAggregate {
        return SubmissionAggregate(
            submissionDAO.load(id),
            submissionFileDAO.listBySubmissionId(id),
            testResultDAO.listBySubmissionId(id)
        )
    }

    fun create(submissionAggregate: SubmissionAggregate) {
        val newSubmissionId = submissionDAO.create(submissionAggregate.submission)

        val submissionFileList = submissionAggregate.submissionFiles
        for (submissionFile in submissionFileList) {
            submissionFileDAO.create(submissionFile.copy(submissionId = newSubmissionId))
        }

        val testResultList = submissionAggregate.testResults
        for (testResult in testResultList) {
            testResultDAO.create(testResult.copy(submissionId = newSubmissionId))
        }
    }

    fun save(submissionAggregate: SubmissionAggregate) {
        val testResultList = submissionAggregate.testResults
        for (testResult in testResultList) {
            if (testResult.id == 0L) {
                testResultDAO.create(testResult)
            } else {
                testResultDAO.update(testResult)
            }
        }
    }
}

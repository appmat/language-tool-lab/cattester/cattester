package ru.appmat.submission.repository

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.update
import ru.appmat.submission.domain.TestResult

internal class TestResultDAO(private val testResultTable: TestResultTable) {
    private fun ResultRow.toTestResult() = TestResult(
        id = this[TestResultTable.id].value,
        testId = this[TestResultTable.testId],
        submissionId = this[TestResultTable.submissionId],
        resultStatus = this[TestResultTable.resultStatus],
        dateTime = this[TestResultTable.dateTime],
        duration = this[TestResultTable.duration],
        errorTitle = this[TestResultTable.errorTitle],
        errorOutput = this[TestResultTable.errorOutput],
        executionStatus = this[TestResultTable.executionStatus],
        profileStatistics = this[TestResultTable.profileStatistics],
        metrics = this[TestResultTable.metrics]
    )

    private fun UpdateBuilder<*>.fromTestResult(testResult: TestResult) {
        this[TestResultTable.testId] = testResult.testId
        this[TestResultTable.submissionId] = testResult.submissionId
        this[TestResultTable.resultStatus] = testResult.resultStatus
        this[TestResultTable.dateTime] = testResult.dateTime
        this[TestResultTable.duration] = testResult.duration
        this[TestResultTable.errorTitle] = testResult.errorTitle
        this[TestResultTable.errorOutput] = testResult.errorOutput
        this[TestResultTable.executionStatus] = testResult.executionStatus
        this[TestResultTable.profileStatistics] = testResult.profileStatistics
        this[TestResultTable.metrics] = testResult.metrics
    }

    fun listBySubmissionId(submission_id: Long) =
        testResultTable.select {
            TestResultTable.submissionId eq submission_id
        }.mapNotNull {
            it.toTestResult()
        }

    fun create(testResult: TestResult) =
        testResultTable.insertAndGetId {
            it.fromTestResult(testResult)
        }.value

    fun update(testResult: TestResult) =
        testResultTable.update({
            TestResultTable.id eq testResult.id
        }) {
            it.fromTestResult(testResult)
        }
}

package ru.appmat.submission.repository

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import ru.appmat.submission.domain.Submission

internal class SubmissionDAO(private val submissionTable: SubmissionTable) {
    private fun ResultRow.toSubmission() = Submission(
        id = this[SubmissionTable.id].value,
        problemId = this[SubmissionTable.problemId]
    )

    private fun UpdateBuilder<*>.fromSubmission(submission: Submission) {
        this[SubmissionTable.problemId] = submission.problemId
    }

    fun load(id: Long) =
        submissionTable.select {
            SubmissionTable.id eq id
        }.single().toSubmission()

    fun listByProblemId(problem_id: Long) =
        submissionTable.select {
            SubmissionTable.problemId eq problem_id
        }.mapNotNull {
            it.toSubmission()
        }

    fun create(submission: Submission) =
        submissionTable.insertAndGetId {
            it.fromSubmission(submission)
        }.value
}

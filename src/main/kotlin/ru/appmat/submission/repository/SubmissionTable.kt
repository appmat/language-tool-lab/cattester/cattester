package ru.appmat.submission.repository

import org.jetbrains.exposed.dao.id.LongIdTable

internal object SubmissionTable: LongIdTable() {
    val problemId = long("problem_id")
}

package ru.appmat.problem_statement.repository

import org.jetbrains.exposed.dao.id.LongIdTable

internal object TestTable: LongIdTable() {
    val index = integer("index")
    val problemId = long("problem_id")
    val input = text("input")
    val expectedOutput = text("expected_output")
    val public = bool("public")
}

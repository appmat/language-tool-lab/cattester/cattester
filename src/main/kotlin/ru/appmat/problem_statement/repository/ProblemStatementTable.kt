package ru.appmat.problem_statement.repository

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.timestamp

internal object ProblemStatementTable: LongIdTable("problem_statement") {
    val description = text("description")
    val lastUpdateTime = timestamp("last_update_time")
}

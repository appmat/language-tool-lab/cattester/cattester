package ru.appmat.problem_statement.repository

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import ru.appmat.problem_statement.domain.Test

internal class TestDAO(private val testTable: TestTable) {
    private fun ResultRow.toTest() = Test(
        id = this[TestTable.id].value,
        index = this[TestTable.index],
        problemId = this[TestTable.problemId],
        input = this[TestTable.input],
        expectedOutput = this[TestTable.expectedOutput],
        public = this[TestTable.public]
    )

    private fun UpdateBuilder<*>.fromTest(test: Test) {
        this[TestTable.index] = test.index
        this[TestTable.problemId] = test.problemId
        this[TestTable.input] = test.input
        this[TestTable.expectedOutput] = test.expectedOutput
        this[TestTable.public] = test.public
    }

    fun listByProblemId(problem_id: Long) =
        testTable.select {
            TestTable.problemId eq problem_id
        }.orderBy(TestTable.index)
            .mapNotNull {
                it.toTest()
            }

    fun create(test: Test) =
        testTable.insertAndGetId {
            it.fromTest(test)
        }.value

    fun update(test: Test) =
        testTable.update({
            TestTable.id eq test.id
        }) {
            it.fromTest(test)
        }

    fun delete(id: Long) =
        testTable.deleteWhere {
            TestTable.id eq id
        }
}

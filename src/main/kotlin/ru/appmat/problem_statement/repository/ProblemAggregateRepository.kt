package ru.appmat.problem_statement.repository

import ru.appmat.problem_statement.domain.ProblemAggregate

internal class ProblemAggregateRepository(
    private val problemStatementDAO: ProblemStatementDAO,
    private val testDAO: TestDAO
) {
    fun list(): List<ProblemAggregate> {
        return problemStatementDAO.list().map {
            ProblemAggregate(
                it,
                testDAO.listByProblemId(it.id)
            )
        }
    }

    fun load(id: Long): ProblemAggregate {
        return ProblemAggregate(
            problemStatementDAO.load(id),
            testDAO.listByProblemId(id)
        )
    }

    fun create(problemAggregate: ProblemAggregate) {
        problemStatementDAO.create(problemAggregate.problemStatement)
    }

    fun save(problemAggregate: ProblemAggregate) {
        problemStatementDAO.update(problemAggregate.problemStatement)

        val testList = problemAggregate.tests
        for (test in testList) {
            if (test.id == 0L) {
                testDAO.create(test)
            } else {
                testDAO.update(test)
            }
        }
    }

    fun deleteTest(id: Long) {
        testDAO.delete(id)
    }
}

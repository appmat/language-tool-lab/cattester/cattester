package ru.appmat.problem_statement.repository

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import ru.appmat.problem_statement.domain.ProblemStatement

internal class ProblemStatementDAO(private val problemStatementTable: ProblemStatementTable) {
    private fun ResultRow.toProblemStatement() = ProblemStatement(
        id = this[ProblemStatementTable.id].value,
        description = this[ProblemStatementTable.description],
        lastUpdateTime = this[ProblemStatementTable.lastUpdateTime]
    )

    private fun UpdateBuilder<*>.fromProblemStatement(problemStatement: ProblemStatement) {
        this[ProblemStatementTable.description] = problemStatement.description
        this[ProblemStatementTable.lastUpdateTime] = problemStatement.lastUpdateTime
    }

    fun load(id: Long) =
        problemStatementTable.select {
            ProblemStatementTable.id eq id
        }.single().toProblemStatement()

    fun list() =
        problemStatementTable.selectAll()
            .mapNotNull {
                it.toProblemStatement()
            }

    fun create(problemStatement: ProblemStatement) =
        problemStatementTable.insertAndGetId {
            it.fromProblemStatement(problemStatement)
        }.value

    fun update(problemStatement: ProblemStatement) =
        problemStatementTable.update({
            ProblemStatementTable.id eq problemStatement.id
        }) {
            it.fromProblemStatement(problemStatement)
        }
}

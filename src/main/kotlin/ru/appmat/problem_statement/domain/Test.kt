package ru.appmat.problem_statement.domain

internal data class Test(
    val id: Long,
    val index: Int,
    val problemId: Long,
    val input: String,
    val expectedOutput: String,
    val public: Boolean
)

package ru.appmat.problem_statement.domain

import java.time.Instant

internal data class ProblemStatement(
    val id: Long,
    val description: String,
    val lastUpdateTime: Instant = Instant.now()
)

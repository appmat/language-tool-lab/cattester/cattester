package ru.appmat.problem_statement.domain

internal data class ProblemStatementRequest(
    val id: Long,
    val description: String
) {
    fun toProblemStatement(): ProblemStatement {
        return ProblemStatement(id, description)
    }
}

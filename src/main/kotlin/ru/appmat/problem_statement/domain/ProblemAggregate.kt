package ru.appmat.problem_statement.domain

import java.time.Instant

internal data class ProblemAggregate(
    val problemStatement: ProblemStatement,
    val tests: List<Test>
) {
    fun getTest(testId: Long): Test {
        return tests.first { it.id == testId }
    }

    fun addTest(test: Test) =
        copy(
            problemStatement = problemStatement.copy(lastUpdateTime = Instant.now()),
            tests = tests.plus(test.copy(index = tests.size, problemId = problemStatement.id))
        )

    fun updateTest(test: Test): ProblemAggregate {
        val previousTest = tests.first { it.id == test.id }

        val newTests = if (test.index == previousTest.index) {
            tests.map {
                if (it.index == test.index) test.copy(problemId = problemStatement.id)
                else it
            }
        } else {
            val testList = tests.toMutableList()
            testList.removeAt(previousTest.index)
            testList.add(test.index, test.copy(problemId = problemStatement.id))
            testList.mapIndexed { i, newTest -> newTest.copy(index = i) }
        }
        return copy(
            problemStatement = problemStatement.copy(lastUpdateTime = Instant.now()),
            tests = newTests
        )
    }

    fun deleteTest(testId: Long): ProblemAggregate {
        val newTests = tests.filterNot { it.id == testId }.mapIndexed { i, newTest -> newTest.copy(index = i) }
        return copy(
            problemStatement = problemStatement.copy(lastUpdateTime = Instant.now()),
            tests = newTests
        )
    }

    fun updateProblemStatement(problemStatement: ProblemStatement): ProblemAggregate {
        require(problemStatement.id == this.problemStatement.id) { "Can't update problem with another id" }
        return copy(problemStatement = problemStatement.copy(lastUpdateTime = Instant.now()))
    }
}

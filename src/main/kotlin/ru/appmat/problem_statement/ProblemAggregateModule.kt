package ru.appmat.problem_statement

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.locations.post
import io.ktor.locations.put
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API
import ru.appmat.problem_statement.domain.ProblemAggregate
import ru.appmat.problem_statement.domain.ProblemStatementRequest
import ru.appmat.problem_statement.domain.Test
import ru.appmat.problem_statement.repository.*

@Suppress("unused")
fun Application.problemAggregateModule() {
    val problemService = ProblemAggregateRepository(ProblemStatementDAO(ProblemStatementTable), TestDAO(TestTable))
    routing {
        problemAggregate(problemService)
    }
}

internal fun Routing.problemAggregate(problemAggregateRepository: ProblemAggregateRepository) {
    get<API.Problems> {
        val problemList = transaction {
            problemAggregateRepository.list().map { it.problemStatement }
        }
        call.respond(problemList)
    }

    post<API.Problems> {
        val problem = call.receive<ProblemStatementRequest>()
        transaction {
            problemAggregateRepository.create(ProblemAggregate(problem.toProblemStatement(), emptyList()))
        }
        call.respond(Created)
    }

    get<API.Problems.Problem> {
        val problem = transaction {
            problemAggregateRepository.load(it.problemId).problemStatement
        }
        call.respond(problem)
    }

    put<API.Problems.Problem> {
        val problem = call.receive<ProblemStatementRequest>()
        transaction {
            var problemAggregate = problemAggregateRepository.load(it.problemId)
            problemAggregate =
                problemAggregate.updateProblemStatement(problem.toProblemStatement().copy(id = it.problemId))
            problemAggregateRepository.save(problemAggregate)
        }
        call.respond(OK)
    }

    get<API.Problems.Problem.Tests> {
        val testList = transaction {
            problemAggregateRepository.load(it.problem.problemId).tests
        }
        call.respond(testList)
    }

    post<API.Problems.Problem.Tests> {
        val test = call.receive<Test>()
        transaction {
            var problemAggregate = problemAggregateRepository.load(it.problem.problemId)
            problemAggregate = problemAggregate.addTest(test)
            problemAggregateRepository.save(problemAggregate)
        }
        call.respond(Created)
    }

    get<API.Problems.Problem.Tests.Public> {
        val testList = transaction {
            problemAggregateRepository.load(it.tests.problem.problemId).tests.filter { it.public }
        }
        call.respond(testList)
    }

    get<API.Problems.Problem.Tests.Test> {
        val test = transaction {
            problemAggregateRepository.load(it.tests.problem.problemId).getTest(it.testId)
        }
        call.respond(test)
    }

    put<API.Problems.Problem.Tests.Test> {
        val test = call.receive<Test>()
        transaction {
            var problemAggregate = problemAggregateRepository.load(it.tests.problem.problemId)
            problemAggregate = problemAggregate.updateTest(test.copy(id = it.testId))
            problemAggregateRepository.save(problemAggregate)
        }
        call.respond(OK)
    }

    delete<API.Problems.Problem.Tests.Test> {
        transaction {
            var problemAggregate = problemAggregateRepository.load(it.tests.problem.problemId)
            problemAggregate = problemAggregate.deleteTest(it.testId)
            problemAggregateRepository.deleteTest(it.testId)
            problemAggregateRepository.save(problemAggregate)
        }
        call.respond(OK)
    }
}

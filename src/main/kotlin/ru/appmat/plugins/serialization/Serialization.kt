package ru.appmat.plugins.serialization

import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*

fun Application.configureSerialization(): ObjectMapper {

    install(DataConversion)

    val mapper = ObjectMapper().apply {
        enable(SerializationFeature.INDENT_OUTPUT)
        disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        registerModule(JavaTimeModule())
        registerKotlinModule()
    }
    val converter = JacksonConverter(mapper)
    install(ContentNegotiation) {
        register(ContentType.Application.Json, converter)
    }
    return mapper
}

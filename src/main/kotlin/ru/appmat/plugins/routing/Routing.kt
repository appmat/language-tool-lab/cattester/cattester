package ru.appmat.plugins.routing

import io.ktor.application.*
import io.ktor.locations.*

fun Application.configureRouting() {
    install(Locations)
}

package ru.appmat.plugins.cors

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*

fun Application.configureCORS() {
    install(CORS) {
        allowNonSimpleContentTypes = true
        host("gitlab.com", schemes = listOf("https"))
        host("cattester.appmat.org", schemes = listOf("https"))
        header(HttpHeaders.ContentType)
        header(HttpHeaders.Authorization)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
    }
}

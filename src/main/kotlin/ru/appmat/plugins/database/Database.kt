package ru.appmat.plugins.database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import ktor_health_check.Health
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.postgresql.PGProperty

fun Application.configureDatabase(testing: Boolean) {
    val hikariDataSource = HikariDataSource(
        HikariConfig().apply {
            val hikariAppConfig = environment.config.config("ktor.hikari")

            when (val gitlabDatabaseUrl = hikariAppConfig.propertyOrNull("gitlabDatabaseUrl")) {
                null -> {
                    username = hikariAppConfig.property("user").getString()
                    password = hikariAppConfig.property("password").getString()
                    jdbcUrl = hikariAppConfig.property("jdbcUrl").getString()
                }
                else -> {
                    // see https://docs.gitlab.com/ee/topics/autodevops/customize.html#using-external-postgresql-database-providers
                    val gitlabDatabaseUrlRegex = Regex(
                        "^postgres://(?<username>[^:]*):(?<password>.*)@(?<address>.*)$"
                    )
                    val matchResult =
                        checkNotNull(gitlabDatabaseUrlRegex.matchEntire(gitlabDatabaseUrl.getString())) {
                            """Database url should have format "postgres://user:password@host:port/database""""
                        }
                    val (user, pass, address) = matchResult.destructured
                    username = user
                    password = pass
                    jdbcUrl = "jdbc:postgresql://${address}"
                }
            }

            hikariAppConfig.propertyOrNull("maximumPoolSize")?.let {
                maximumPoolSize = it.getString().toInt()
            }
            hikariAppConfig.propertyOrNull("sslMode")?.let {
                addDataSourceProperty(PGProperty.SSL_MODE.getName(), it.getString())
            }

            validate()
        }

    )

    val flyway = Flyway.configure().dataSource(hikariDataSource).load()
    if (testing) {
        flyway.clean()
    }
    flyway.migrate()
    Database.connect(hikariDataSource)

    featureOrNull(Health)?.cfg?.apply {
        readyCheck("database") {
            hikariDataSource.isRunning
        }
    }
}

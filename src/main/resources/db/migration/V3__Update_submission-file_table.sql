ALTER TABLE submission_file
    ALTER COLUMN file TYPE bytea USING file::bytea;

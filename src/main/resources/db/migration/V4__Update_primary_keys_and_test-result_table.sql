ALTER SEQUENCE submission_id_seq as bigint;
ALTER TABLE submission
    ALTER COLUMN id TYPE bigint;

ALTER SEQUENCE submission_file_id_seq as bigint;
ALTER TABLE submission_file
    ALTER COLUMN id TYPE bigint;

ALTER SEQUENCE test_result_id_seq as bigint;
ALTER TABLE test_result
    ALTER COLUMN id TYPE bigint,
    DROP COLUMN duration,
    ADD COLUMN duration bigint NOT NULL;

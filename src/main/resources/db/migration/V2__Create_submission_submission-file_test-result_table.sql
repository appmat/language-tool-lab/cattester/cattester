CREATE TABLE submission (
    id SERIAL PRIMARY KEY,
    problem_id int references problem_statement(id)
);

CREATE TABLE submission_file (
    id SERIAL PRIMARY KEY,
    submission_id int references submission(id),
    name text NOT NULL,
    file text NOT NULL
);

CREATE TABLE test_result (
    id SERIAL PRIMARY KEY,
    test_id int references test(id),
    submission_id int references submission(id),
    result_status varchar(20),
    date_time timestamp NOT NULL,
    duration interval MINUTE TO SECOND NOT NULL,
    error_output text,
    error_title varchar(20),
    execution_status varchar(20),
    profile_statistics text,
    metrics text
);

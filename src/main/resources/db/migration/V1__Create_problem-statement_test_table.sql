CREATE TABLE problem_statement (
    id BIGSERIAL PRIMARY KEY,
    description text NOT NULL,
    last_update_time timestamp NOT NULL
);

CREATE TABLE test (
    id BIGSERIAL PRIMARY KEY,
    index int NOT NULL,
    problem_id bigint references problem_statement(id),
    input text,
    expected_output text NOT NULL,
    public boolean
);

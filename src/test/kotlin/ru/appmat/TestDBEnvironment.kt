package ru.appmat

import io.ktor.config.*
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

private class AppPostgreSQLContainer : PostgreSQLContainer<AppPostgreSQLContainer>(
    DockerImageName.parse("postgres:13")
)

private const val ENV_GITLAB_DATABASE_URL = "DATABASE_URL"

object TestDBEnvironment {

    private val postgreSQLContainer = AppPostgreSQLContainer()
    private val databaseUrl: String? by lazy { System.getenv(ENV_GITLAB_DATABASE_URL) }
    private val needTestcontainersDatabase
        get() = databaseUrl == null

    fun initialize() {
        if (needTestcontainersDatabase && !postgreSQLContainer.isRunning) {
            postgreSQLContainer.start()
        }
    }

    fun enrichAppConfig(config: MapApplicationConfig) {
        with(config) {
            if (needTestcontainersDatabase) {
                put("ktor.hikari.user", postgreSQLContainer.username)
                put("ktor.hikari.password", postgreSQLContainer.password)
                put("ktor.hikari.jdbcUrl", postgreSQLContainer.jdbcUrl)
            } else {
                put("ktor.hikari.gitlabDatabaseUrl", requireNotNull(databaseUrl))
            }
        }
    }

}

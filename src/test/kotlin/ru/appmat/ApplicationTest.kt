package ru.appmat

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.ktor.config.*
import io.ktor.http.*
import io.ktor.server.testing.*
import ru.appmat.problem_statement.domain.ProblemStatementRequest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {

    @BeforeTest
    fun initDB() {
        TestDBEnvironment.initialize()
    }

    private val testEnvironment by lazy {
        createTestEnvironment {
            val appConfig = config
            check(appConfig is MapApplicationConfig) { "Could not initialize app context" }
            TestDBEnvironment.enrichAppConfig(appConfig)
        }
    }

    @Test
    fun createProblem() = withApplication(testEnvironment) {
        with(application) {
            module(testing = true)
        }

        with(handleRequest(HttpMethod.Post, "/api/problems") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(
                ObjectMapper().registerModule(JavaTimeModule()).writeValueAsString(
                    ProblemStatementRequest(
                        0,
                        "test"
                    )
                )
            )
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
        }
    }

}

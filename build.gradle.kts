import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometheus_version: String by project
val exposed_version: String by project
val flyway_version: String by project
val postgresql_version: String by project
val hikari_version: String by project
val koin_version: String by project
val testcontainers_version: String by project
val jacksonjsr310_version: String by project

plugins {
    application
    kotlin("jvm")
}

group = "ru.appmat"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

java {
    targetCompatibility = JavaVersion.VERSION_16
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    implementation(platform("org.testcontainers:testcontainers-bom:$testcontainers_version"))
    implementation("com.github.zensum:ktor-health-check:011a5a8")

    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-sessions:$ktor_version")
    implementation("io.ktor:ktor-jackson:$ktor_version")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonjsr310_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometheus_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")

    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
    testImplementation("org.testcontainers:postgresql")

    implementation("org.flywaydb:flyway-core:$flyway_version")
    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")
    implementation("org.postgresql:postgresql:$postgresql_version")
    implementation("com.zaxxer:HikariCP:$hikari_version")
    implementation("io.insert-koin:koin-core:$koin_version")
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")

    testImplementation("io.insert-koin:koin-test:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit4:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit5:$koin_version")
}

tasks {
    withType<KotlinCompile>().all {
        kotlinOptions {
            jvmTarget = java.targetCompatibility.toString()
            freeCompilerArgs = listOf(
                "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
            )
        }
    }

    withType<Test>().all {
        jvmArgs = listOf("--add-opens=java.base/java.time=ALL-UNNAMED")
    }
}

application {
    applicationDefaultJvmArgs = listOf(
        "--add-opens=java.base/java.time=ALL-UNNAMED"
    )
}

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(project.projectDir)
        File("Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}
